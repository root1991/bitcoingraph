package com.example.root.bitcoingraph

import com.example.root.bitcoingraph.entity.ChartData
import com.example.root.bitcoingraph.entity.ChartPoint
import com.example.root.bitcoingraph.repo.ChartRepo
import com.example.root.bitcoingraph.repo.network.ChartRemoteDataSource
import com.example.root.bitcoingraph.repo.toLocalEntity
import io.mockk.coEvery
import com.example.root.bitcoingraph.GraphPointQueries as ChartLocalDataSource
import io.mockk.mockk
import io.mockk.verify
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

class ChartRepoTest {
    private val remoteDataSource = mockk<ChartRemoteDataSource>()
    private val localDataSource = mockk<ChartLocalDataSource>(relaxUnitFun = true)
    private val chartPoint = ChartPoint(0L, 0.0f)
    private val chartData = ChartData("name", listOf(chartPoint))

    private val repo = ChartRepo(
        remoteDataSource,
        localDataSource,
        TestCoroutineDispatcher()
    )

    @Test
    fun `getChartData saves queried network chart points in database`() = runBlockingTest {
        coEvery { remoteDataSource.getChartData() } returns chartData

        val contacts = repo.getChartData()

        assertEquals(chartPoint.x, contacts.items.values.first().x)
        assertEquals(chartPoint.y, contacts.items.values.first().y)
        verify {
            localDataSource.insert(chartData.toLocalEntity().first())
        }
    }
}