package com.example.root.bitcoingraph

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.root.bitcoingraph.chart.ChartViewModel
import com.example.root.bitcoingraph.chart.ChartViewModel.ChartState
import com.example.root.bitcoingraph.entity.ChartData
import com.example.root.bitcoingraph.entity.ChartPoint
import com.example.root.bitcoingraph.repo.ChartRepo
import com.example.root.bitcoingraph.repo.Result
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verifyOrder
import junit.framework.Assert.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ChartViewModelTest {
    private val contactRepo = mockk<ChartRepo>()

    private val viewModel = ChartViewModel(contactRepo, 0)
    private val chartData = ChartData("name", listOf(ChartPoint(0L, 0.0f)))


    @get:Rule
    val liveDataRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(TestCoroutineDispatcher())
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `Initial state is displayed after initialization`() {
        coEvery { contactRepo.getChartData() } returns
                Result.Fresh(chartData)

        val observer = mockk<Observer<ChartState>>(relaxUnitFun = true)
        viewModel.state().observeForever(observer)
        viewModel.loadChartData()

        verifyOrder {
            observer.onChanged(ChartState.Loading)
        }
    }

    @Test
    fun `Network error is emitted when Cached Result returns`() = runBlockingTest {
        coEvery { contactRepo.getChartData() } returns Result.Cached(chartData)
        viewModel.loadChartData()

        assertTrue(viewModel.state().value is ChartState.Error)
    }

    @Test
    fun `Success is emitted when Fresh Result refresh fails`() = runBlockingTest {
        coEvery { contactRepo.getChartData() } returns Result.Fresh(chartData)
        viewModel.loadChartData()

        assertTrue(viewModel.state().value is ChartState.Success)
    }
}
