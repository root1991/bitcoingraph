package com.example.root.bitcoingraph.repo

sealed class Result<T> {
    abstract val items: T

    data class Fresh<T>(override val items: T): Result<T>()
    data class Cached<T>(override val items: T): Result<T>()
}