package com.example.root.bitcoingraph.chart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.root.bitcoingraph.entity.ChartData
import com.example.root.bitcoingraph.entity.ChartPoint
import com.example.root.bitcoingraph.repo.ChartRepo
import com.example.root.bitcoingraph.repo.Result
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.coroutines.launch
import java.util.*

class ChartViewModel(private val chartRepo: ChartRepo, private val chartColor: Int) : ViewModel() {

    private val state = MutableLiveData<ChartState>()
    fun state(): LiveData<ChartState> = state

    fun loadChartData() {
        state.value = ChartState.Loading
        viewModelScope.launch {
            when (val result = chartRepo.getChartData()) {
                is Result.Fresh -> handleFreshResult(result)
                is Result.Cached -> handleCachedResult(result)
            }
        }

    }

    private fun handleCachedResult(result: Result<ChartData>) {
        handleFreshResult(result)
        state.value = ChartState.Error(
            errorMessage = "Network error"
        )
    }

    private fun handleFreshResult(result: Result<ChartData>) {
        if (result.items.values.isNotEmpty()) {
            state.value = ChartState.Success(
                setupGraphView(
                    result.items.values,
                    chartColor
                ), result.items.name
            )
        }
    }

    private fun setupGraphView(points: List<ChartPoint> = listOf(), lineColor: Int = 0): LineData {
        val entries = points.map { Entry(Date(it.x).time.toFloat(), it.y) }
        val dataSet = LineDataSet(entries, null).apply {
            color = lineColor
            lineWidth = 1f
            setDrawCircles(false)
            setDrawValues(false)
        }
        return LineData(dataSet)
    }

    sealed class ChartState {
        object Loading : ChartState()
        data class Error(
            val errorMessage: String
        ) : ChartState()

        data class Success(val points: LineData, val chartName: String) : ChartState()
    }
}