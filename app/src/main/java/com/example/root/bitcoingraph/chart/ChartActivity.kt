package com.example.root.bitcoingraph.chart

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.root.bitcoingraph.R
import com.example.root.bitcoingraph.ServiceLocator
import com.example.root.bitcoingraph.chart.ChartViewModel.ChartState.*
import com.github.mikephil.charting.data.LineData
import kotlinx.android.synthetic.main.activity_main.*

class ChartActivity : AppCompatActivity() {

    private val viewModel
            by viewModels<ChartViewModel> { ServiceLocator.viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.loadChartData()

        viewModel.state().observe(this, Observer { state ->
            when (state) {
                is Success -> showChart(state.points, state.chartName)
                is Loading -> showLoading()
                is Error -> showError(state.errorMessage)
            }
        })
    }

    private fun showChart(lineData: LineData, charTitle: String) {
        chartView.data = lineData
        chartView.xAxis.setDrawLabels(false)

        loadingIndicator.visibility = View.GONE

        chartData.visibility = View.VISIBLE

        chartName.text = charTitle
        chartView.invalidate()
    }

    private fun showLoading() {
        chartData.visibility = View.GONE
        loadingIndicator.visibility = View.VISIBLE
    }

    private fun showError(message: String) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .create().apply {
                setTitle(getString(R.string.error_dialog_title))
            }.show()
    }
}
