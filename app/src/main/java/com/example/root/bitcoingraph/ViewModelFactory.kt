package com.example.root.bitcoingraph

import android.app.Application
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.root.bitcoingraph.chart.ChartViewModel
import com.example.root.bitcoingraph.repo.ChartRepo

class ViewModelFactory(
    private val contactRepo: ChartRepo,
    private val application: Application
) : ViewModelProvider.AndroidViewModelFactory(application) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when (modelClass) {
        ChartViewModel::class.java -> ChartViewModel(
            contactRepo, ContextCompat.getColor(application, R.color.colorAccent)
        )
        else -> error("Unknown view model class $modelClass")
    } as T
}