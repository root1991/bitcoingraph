package com.example.root.bitcoingraph.repo

import com.example.root.bitcoingraph.entity.ChartData
import com.example.root.bitcoingraph.entity.ChartPoint
import com.example.root.bitcoingraph.repo.network.ChartRemoteDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import com.example.root.bitcoingraph.GraphPoint as LocalEntity
import com.example.root.bitcoingraph.GraphPointQueries as ChartLocalDataSource

class ChartRepo(
    private val remoteDataSource: ChartRemoteDataSource,
    private val localDataSource: ChartLocalDataSource,
    private val dispatcher: CoroutineDispatcher
) {
    suspend fun getChartData() = try {
        Result.Fresh(getChartsFromNetwork())
    } catch (exception: Exception) {
        Result.Cached(getChartsFromDataBase())
    }

    private suspend fun getChartsFromNetwork(): ChartData =
        remoteDataSource.getChartData()
            .toLocalEntity()
            .onEach {
                localDataSource.insert(it)
            }
            .toEntity()

    private suspend fun getChartsFromDataBase() = withContext(dispatcher) {
        localDataSource.selectAll().executeAsList().toEntity()
    }

    private fun List<LocalEntity>.toEntity() = if (isNotEmpty()) {
        ChartData(
            name = first().name,
            values = map {
                ChartPoint(
                    it.x.toLong(), it.y.toFloat()
                )
            }
        )
    } else ChartData(name = "", values = listOf())
}

fun ChartData.toLocalEntity(): List<LocalEntity> = values.map {
    LocalEntity.Impl(
        name = name,
        x = it.x.toString(),
        y = it.y.toString()
    )
}

