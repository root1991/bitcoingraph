package com.example.root.bitcoingraph.repo.network

import com.example.root.bitcoingraph.entity.ChartData
import retrofit2.http.GET

interface ChartRemoteDataSource {

    @GET("charts/market-price?sampled=true")
    suspend fun getChartData(): ChartData

}