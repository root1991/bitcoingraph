package com.example.root.bitcoingraph

import com.example.root.bitcoingraph.repo.ChartRepo
import com.example.root.bitcoingraph.repo.network.ChartRemoteDataSource
import com.squareup.sqldelight.android.AndroidSqliteDriver
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.example.root.bitcoingraph.GraphPointQueries as ChartLocalDataSource

object ServiceLocator {

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl("https://api.blockchain.info/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val viewModelFactory by lazy {
        ViewModelFactory(chartRepo, App.app)
    }

    private val database: Database by lazy {
        Database(AndroidSqliteDriver(Database.Schema, App.app.applicationContext, "contacts.db"))
    }

    private val chartLocalDataSource: ChartLocalDataSource get() = database.graphPointQueries

    private val chartRemoteDataSource: ChartRemoteDataSource
        get() = retrofit.create(ChartRemoteDataSource::class.java)

    private val chartRepo: ChartRepo
        get() = ChartRepo(
            chartRemoteDataSource,
            chartLocalDataSource,
            Dispatchers.IO
        )
}