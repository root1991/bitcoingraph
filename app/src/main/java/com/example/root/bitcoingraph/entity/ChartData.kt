package com.example.root.bitcoingraph.entity

class ChartData(
    val name: String,
    val values: List<ChartPoint>
)